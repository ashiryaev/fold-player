function assignHandlers() {
    $('#linkSave').hide();

    $('#btn_search').click(function() {
        window.location = "/#search?q=" + encodeURIComponent($('#text_search').val());
    });
    
    $('#text_search').keypress(function(e){
        if (e.which == 13) {
            window.location = "/#search?q=" + encodeURIComponent($('#text_search').val());
        }
    });

    $('#linkSave').click(function () {
        var hash = getRouteObject();
        saveAlbum(
            decodeURIComponent(hash.params[0].value),
            decodeURIComponent(hash.params[1].value));
        return false;
    });

    $(window).hashchange(function() {
        var hash = getRouteObject();

        if (hash === null) {
            home();
            return;
        }

        switch (hash.name) {
        case "search":
            $('#text_search').val(decodeURIComponent(hash.params[0].value));
            search(hash.params[0].value);
            break;

        case "artist":
            $('#text_search').val(decodeURIComponent(hash.params[0].value));
            searchAlbums(hash.params[0].value);
            break;            

        case "album":
            $('#text_search').val(decodeURIComponent(hash.params[0].value));
            searchSongs(hash.params[0].value, hash.params[1].value);
            break;            

        default:
            window.location = "/#";
        }
    });

    $(window).hashchange();
}

function play(track) {
    audio.load(track.url);
    audio.play();
}

function setupPlayer() {
    $('tr.song').click(function(){
        if ($(this).hasClass('playing')) {
            audio.playPause();
        } else {
            $(this).addClass('playing').siblings().removeClass('playing');

            var hash = getRouteObject();        
            var song = $('td', this).eq(1).text();
            var artist = decodeURIComponent(hash.params[0].value);
            searchVKTrack(artist, song, play);
        }
    });
}

function home(){}

function nextTrack() {
    var next = $('#search_results tr.playing').next();
    if (!next.length) next = $('#search_results tr').first();
    next.addClass('playing').siblings().removeClass('playing');
    
    var hash = getRouteObject();        
    var song = $('td', next).eq(1).text();
    var artist = decodeURIComponent(hash.params[0].value);
    searchVKTrack(artist, song, play);
}

function prevTrack() {
    var prev = $('#search_results tr.playing').prev();
    if (!prev.length) prev = $('#search_results tr').last();
    prev.addClass('playing').siblings().removeClass('playing');
    
    var hash = getRouteObject();
    var song = $('td', prev).eq(1).text();
    var artist = decodeURIComponent(hash.params[0].value);
    searchVKTrack(artist, song, play);
}

function initPlayer(){
    // Setup the player to autoplay the next track
    var a = audiojs.createAll({
        trackEnded: function() {
            nextTrack();
        },
        createPlayer: {
            markup: '\
<div class="play-pause"> \
  <p class="play"></p> \
  <p class="pause"></p> \
  <p class="loading"></p> \
  <p class="error"></p> \
</div> \
<div class="play-control"> \
  <p class="prev"></p> \
</div> \
<div class="play-control"> \
  <p class="next"></p> \
</div> \
<div class="scrubber"> \
  <div class="progress"></div> \
  <div class="loaded"></div> \
</div> \
<div class="time"> \
  <em class="played">00:00</em>/<strong class="duration">00:00</strong> \
</div> \
<div class="error-message"></div>',
        playPauseClass: 'play-pause',
        scrubberClass: 'scrubber',
        progressClass: 'progress',
        loaderClass: 'loaded',
        timeClass: 'time',
        durationClass: 'duration',
        playedClass: 'played',
        errorMessageClass: 'error-message',
        playingClass: 'playing',
        loadingClass: 'loading',
        errorClass: 'error'
        }    
    });
    
    // Load in the first track
    audio = a[0];

    $('.play-control .next').click(function () {
        nextTrack();
    });

    $('.play-control .prev').click(function () {
        prevTrack();
    });

    // Keyboard shortcuts
    $(document).keydown(function(e) {
        var unicode = e.charCode ? e.charCode : e.keyCode;
        if (e.altKey) {
            return;
        }
        // right arrow
        if (unicode == 39) {
            nextTrack();
            // back arrow
        } else if (unicode == 37) {
            prevTrack();
            // p
        } else if (unicode == 80) {
            audio.playPause();
        }
    });

    $('#text_search').keydown(function(e) {
        e.stopPropagation();
        e.cancelBubble = true;
    });
}

function searchSongs(artist, albumName) {
    $.ajax({
        url: "/lfm?method=album.getInfo" + 
                   "&artist=" + encodeURIComponent(artist) +
                   "&album=" + encodeURIComponent(albumName),
        context: document.body
    }).done(function(data) { 
        try {
            var songs = data.album.tracks.track;
            if (songs.length > 0){
                var search_results = $('#search_results');
                var tbl = $('table', search_results).empty();
                for (var i = 0; i < songs.length; i++) {
                    var minutes = Math.floor(songs[i].duration / 60);
                    var seconds = songs[i].duration % 60;
                    songs[i].humanDuration = 
                        minutes.toString() +
                        ":" + (seconds < 10 ? '0' : '') + seconds.toString();
                    var song = templates.songs.render(songs[i]);
                    tbl.append(song);
                }

                setupPlayer();
            }
        }
        catch(e) {
            console.log(e);
        }
    });    
}

function searchAlbums(text) {
    $.ajax({
        url: "/lfm?method=artist.getTopAlbums&artist=" + encodeURIComponent(text),
        context: document.body
    }).done(function(data) { 
        try {
            var albums = data.topalbums.album;
            if (albums.length > 0){
                var search_results = $('#search_results');
                var tbl = $('table', search_results).empty();
                for (var i = 0; i < albums.length; i++) {
                    albums[i].image[2].show = true;
                    albums[i].nameURI = encodeURIComponent(albums[i].name);
                    albums[i].artist.nameURI = encodeURIComponent(albums[i].artist.name);
                    var album = templates.albums.render(albums[i]);
                    tbl.append(album);
                }
            }
        }
        catch(e) {
            console.log(e);
        }
    });
}

function search(text) {
    $.ajax({
        url: "/lfm?method=artist.search&artist=" + encodeURIComponent(text),
        context: document.body
    }).done(function(data) { 
        try {
            var artists;
            var total = Number(data.results["opensearch:totalResults"]);
            if (total <= 0) {
                return;
            } else if (total > 1) {
                artists = data.results.artistmatches.artist;                
            } else {
                artists = [data.results.artistmatches.artist];
            }

            if (artists.length > 0){
                var search_results = $('#search_results');
                var tbl = $('table', search_results).empty();
                for (var i = 0; i < artists.length; i++) {
                    artists[i].image[2].show = true;
                    artists[i].nameURI = encodeURIComponent(artists[i].name);
                    var artist = templates.artists.render(artists[i]);
                    tbl.append(artist);
                }
            }
        }
        catch(e) {
            console.log(e);
        }
    });    
}

function vkRequest(successCallback, failedCallback, method, pName1, pValue1, pName2, pValue2) {
    var cookies, i, token, cookie, query;
 
    cookies = document.cookie.split(";")
    for (i = 0; i <= cookies.length; i++) {
        cookie = cookies[i].split("=");
        if (cookie[0] === 'vklogin') {
            token = cookie[1].substring(0, cookie[1].indexOf("|"));
            break;
        }
    }

    if (pName1 && pValue1) {
        query = "?" + pName1 + "=" + encodeURI(pValue1);
    }

    if (pName2 && pValue2) {
        query += "&" + pName2 + "=" + encodeURI(pValue2);
    }

    $.ajax({
        url: "https://api.vk.com/method/" + 
            method + query
            + "&access_token=" + token,
        context: document.body,
        dataType: 'jsonp'
    }).done(function(data) {
        if (successCallback) {
            successCallback(data);
        }
    }).error(function() {
        if (failedCallback) {
            failedCallback();
        }
    });
}

function searchVKTrack(artist, song, callback, error){
    var text;
    
    if (song.indexOf("(") > 0 &&
        song.indexOf("(") < song.indexOf(")")) {
        song = song.substring(0, song.indexOf("(")) +
            song.substring(song.indexOf(")") + 1);
    } else {
        song = song.replace("(", " ").replace(")", " ");    
    }
    
    text = artist + " - " + song;
    vkRequest(success, error, "audio.search", "q", text);

    function success(data) { 
        var obj = data;
        if (obj.response.length > 1){
            var origTracks = obj.response.slice(1);
            var tracks = jQuery.grep(origTracks, function(track) {
                return track.artist == artist;
            });
            if (tracks.length == 0) {
                tracks = jQuery.grep(origTracks, function(track) {
                    return track.artist.indexOf(artist) >= 0;
                });
            }
            tracks = jQuery.grep(tracks, function(track) {
                return track.title.indexOf(song) == 0;
            });

            if (tracks.length > 0) {
                callback(tracks[0]);
                return;
            }
        }
    }
}

function saveAlbum(artist, album) {
    var text = artist + " - " + album;
    vkRequest(success, null, "audio.addAlbum", "title", text);

    function success(data) {
        var albumId = data.response.album_id;
        var songs = 
            $('tr.song').map(function(i, elem){
                return $(elem).children().eq(1).text()
            });

        var tracks = [];
        var myTracks = [];
        moveToAlbum(0);

        function moveToAlbum(i) {
            setTimeout(function () {
                var song = songs[i];
                searchVKTrack(artist, song, function(track) {
                    tracks.push(track);

                    if (i >= (songs.length - 1)) {
                        addSongs(0);
                    }    
                });

                if (i >= (songs.length - 1)) {
                    // DO NOTHING
                } else {
                    moveToAlbum(i+1);
                }            
            }, 5000);
        }

        function addSongs(i) {
            setTimeout(function () {
                var track = tracks[i];

                vkRequest(success, null, 'audio.add', 
                'aid', track.aid,
                'oid', track.owner_id);

                if (i >= (tracks.length - 1)) {
                    // DO NOTHING
                } else {
                    addSongs(i+1, tracks);
                }

                function success(data) {
                    myTracks.push(data.response);

                    if (i >= (tracks.length - 1)) {
                        var songString = myTracks.join(',');  
                        vkRequest(function(data){
                            console.log(data);
                        }, null,
                        'audio.moveToAlbum',
                        'aids', songString,
                        'album_id', albumId);
                    } 
                }
            }, 5000);
        }
    }
}

function getRouteObject() {
    var hash, pair, q, params, paramString;
    if (location.hash.length > 0) {
        hash = location.hash.substr(1);
    } else {
        return null;
    }

    q = hash.indexOf("?");
    if (q >= 0) {
        name = hash.substr(0, q);
        params = [];
        paramString = hash.substr(q + 1);
        if (paramString.length > 0) {
            var arr = paramString.split("&");
            for (var i = 0; i < arr.length; i++) {
                var pair = arr[i].split("=");
                if (pair.length > 1) {
                    params.push({key:pair[0], value: pair[1]});
                } else {
                    params.push({key:pair[0], value: null});
                }
            }
        }
    } else {
        name = hash;
        params = [];
    }
    
    return {name: name, params: params};
}

var audio = null;

// templates
var templates = {};

templates.songs = Hogan.compile('\
<li><a href="#" data-src="{{url}}">{{ artist }} - {{ title }}</a></li>');

templates.artists = Hogan.compile('\
<tr>\
<td>\
\
{{#image}}\
{{#show}}\
  <img src="{{ #text }}"></img>\
{{/show}}\
{{/image}}\
\
</td>\
<td>\
<h2>\
<a href="#artist?name={{ nameURI  }}">{{ name }}</a>\
</h2>\
</tr>');

templates.albums = Hogan.compile('\
<tr>\
<td>\
\
{{#image}}\
{{#show}}\
  <img src="{{ #text }}"></img>\
{{/show}}\
{{/image}}\
\
</td>\
<td>\
<h2>\
<a href="#album?artist={{#artist}}{{ nameURI }}{{/artist}}&name={{ nameURI }}">{{ name }}</a>\
</h2>\
</tr>');

templates.songs = Hogan.compile('\
<tr class="song">\
<td>\
<div class="playIcon"></div>\
</td>\
<td>\
{{ name }}\
</td>\
<td>\
{{ humanDuration }}\
</td>\
</tr>');

$(function() {
    assignHandlers();
    initPlayer();
});