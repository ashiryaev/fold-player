# Fold Player

Web Application for playing music from VK.

## Run

Requirements:

* [Erlang](http://www.erlang.org/download.html);
* If you on Windows, you need [Cygwin](http://www.cygwin.com/install.html) with `make`.

```
git clone https://github.com/*************
cd fold-player
make
make run
```

Go to [http://localhost:5000](http://localhost:5000)