-module(index_handler).
-behaviour(cowboy_http_handler).
-export([init/3, handle/2, terminate/2]).

init({tcp, http}, Req, _Opts) ->
    {ok, Req, undefined_state}.

handle(Req, State) ->
    LoggedIn = is_loggedin(Req),
    %%LoggedIn = true,
    {ok, Body} = index_dtl:render([{loggedIn, LoggedIn}]),
    Headers = [{<<"Content-Type">>, <<"text/html">>}],
    {ok, Req2} = cowboy_http_req:reply(200, Headers, Body, Req),
    {ok, Req2, State}.

is_loggedin(Req) ->
    case cowboy_http_req:cookie(<<"vklogin">>, Req) of
        {undefined, _} ->
            false;
        _ ->
            true
    end.

terminate(_Req, _State) ->
    ok.
