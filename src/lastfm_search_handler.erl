-module(lastfm_search_handler).
-behaviour(cowboy_http_handler).
-include("fold_player_int.hrl").

%% Application callbacks

-export([init/3, handle/2, terminate/2]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

init({_Any, http}, Req, []) ->
	{ok, Req, undefined}.

handle(Req, State) ->
    {Method, _} = cowboy_http_req:qs_val(<<"method">>, Req),

    Query = case Method of
        <<"artist.search">> ->
                    {Param, _} = cowboy_http_req:qs_val(<<"artist">>, Req),
                    DecodedParam = cowboy_http:urldecode(Param),
                    [<<"artist.search&artist=">>, cowboy_http:urlencode(DecodedParam)];

        <<"artist.getTopAlbums">> ->
                    {Param, _} = cowboy_http_req:qs_val(<<"artist">>, Req),
                    DecodedParam = cowboy_http:urldecode(Param),
                    [<<"artist.gettopalbums&artist=">>, cowboy_http:urlencode(DecodedParam)];

        <<"album.getInfo">> ->
                    {Artist, _} = cowboy_http_req:qs_val(<<"artist">>, Req),
                    {Album, _} = cowboy_http_req:qs_val(<<"album">>, Req),
                    DecodedArtist = cowboy_http:urldecode(Artist),
                    DecodedAlbum = cowboy_http:urldecode(Album),
                    [<<"album.getInfo&artist=">>, cowboy_http:urlencode(DecodedArtist), <<"&album=">>, cowboy_http:urlencode(DecodedAlbum)]
    end,

    Url = list_to_binary(
            [
             ?LFM_API_URL,
             Query, 
             <<"&limit=30&format=json">>,
             <<"&api_key=">>, ?LFM_API_KEY]),
    
    erlang:display(binary_to_list(Url)),

    Resp = httpc:request(binary:bin_to_list(Url)),
    Result = 
        case Resp of
            {ok, {{_V, _C, _R}, _H, Body}} -> Body;
            {error, Reason} -> 
                erlang:display(Reason),
                Reason;
            Y -> erlang:display(Y),
                 erlang:throw({unknown, Y})                      
        end,
    
    erlang:display(Result),
	{ok, Req2} = cowboy_http_req:reply(200, [{'Content-Type', <<"application/json">>}],
                                       Result, Req),
	{ok, Req2, State}.

terminate(_Req, _State) ->
	ok.
