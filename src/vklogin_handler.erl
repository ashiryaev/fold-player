-module(vklogin_handler).
-behaviour(cowboy_http_handler).
-include("fold_player_int.hrl").

%% Application callbacks

-export([init/3, handle/2, terminate/2]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

init({_Any, http}, Req, []) ->
	{ok, Req, undefined}.

handle({error, _Error}, State) ->
	{ok, ok, State};

handle(Req, State) ->
    {Code, _Req} = cowboy_http_req:qs_val(<<"code">>, Req),
    {Status, Data} = 
        case Code of
            undefined -> {error, "No code param"};
            _ -> vklogin(Code)
        end,
    
    {ok, Req2} = 
        case Status of 
            error -> cowboy_http_req:reply(200, [{'Content-Type', <<"text/html">>}], [Data], Req);
            ok ->
                {Header, Resp} = Data,
                cowboy_http_req:reply(302, [{<<"Location">>, <<"/">>}, Header], Resp, Req)
        end,
	{ok, Req2, State}.

vklogin(Code) ->
    Url = list_to_binary(
            [?VK_OAUTH_URL,
            <<"?client_id=">>, ?VK_APPID,
            <<"&client_secret=">>, ?VK_SECRET_KEY,
            <<"&code=">>, Code,
            <<"&redirect_uri=http://fold-pleer.tk/vklogin">>]),

    Resp = httpc:request(binary:bin_to_list(Url)),
    Result = 
        case Resp of
            {ok, {{_V, _C, _R}, _H, Body}} -> Body;
            {error, Reason} -> erlang:display(Reason),
                               error;
            X -> erlang:display(X),
                 error                               
        end,

    case Result of
        error -> {error, "Unable to connect to VKontakte"};
        _ -> Token = parse_token(Result),
             case Token of
                 {ok, {token, SecToken, expires, Expires, userid, UserId}} ->
                     Value = list_to_binary([SecToken, <<"|">>, integer_to_list(UserId)]),                   
                     Cookie = cowboy_cookies:cookie(<<"vklogin">>, Value, [{max_age, Expires}]),
                     {ok, {Cookie, SecToken}};
                 {error, Error} ->
                     erlang:display(Error),
                     {error, Error}
             end
    end.

parse_token(JsonString) ->
    Json = mochijson2:decode(JsonString),
    case Json of
        {struct, [
                  {<<"access_token">>, Token},
                  {<<"expires_in">>, Expires},
                  {<<"user_id">>, UserId}]} ->
            {ok, {token, Token, expires, Expires, userid, UserId}};

        {struct, [
                  {<<"error">>, _Error},
                  {<<"error_description">>, ErrorDescription}]} ->
            {error, ErrorDescription};

        _ -> erlang:display(Json),
            {error, "Unable to parse response from VKontakte"}
    end.

terminate(_Req, _State) ->
	ok.
