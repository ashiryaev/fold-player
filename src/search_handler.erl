-module(search_handler).
-behaviour(cowboy_http_handler).
-include("fold_player_int.hrl").

%% Application callbacks

-export([init/3, handle/2, terminate/2]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

init({_Any, http}, Req, []) ->
	{ok, Req, undefined}.

handle(Req, State) ->
    {Query, _Req} = cowboy_http_req:qs_val(<<"q">>, Req),
    {Token, _UserId} = fromCookie(Req),
    Url = list_to_binary(
            [?VK_API_URL,
            <<"audio.search">>,
            <<"?q=">>, Query,
            <<"&access_token=">>, Token]),
    
    Resp = httpc:request(binary:bin_to_list(Url)),
    Result = 
        case Resp of
            {ok, {{_V, _C, _R}, _H, Body}} -> Body;
            {error, Reason} -> Reason;
            X -> erlang:display(X),
                 erlang:throw({unknown, X})                      
        end,
    
	{ok, Req2} = cowboy_http_req:reply(200, [{'Content-Type', <<"application/json">>}],
                                       Result, Req),
	{ok, Req2, State}.

terminate(_Req, _State) ->
	ok.

fromCookie(Req) ->
    case cowboy_http_req:cookie(<<"vklogin">>, Req) of
        {undefined, _} ->
            erlang:throw(no_cookie);
        {Res, _} ->
            case binary:split(Res, <<"|">>, []) of
                [Token|[UserId]] -> {Token, UserId};
                _ -> erlang:throw(invalid_cookie, Res)
            end
    end.
